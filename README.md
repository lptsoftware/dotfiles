# dotfiles
Work-In-Progress Repo of dotfiles for Linux and Mac machines

### Goals
- Cross-Platform
  - Should be able to use any linux distro or Mac os
    - Bonus: Compatibility with Windows on Msys2
- Custom install
  - Should be able to choose between "light" installations for servers or "full" installations for dev environments
- Easy to update
  - Able to pull all updates through one command
